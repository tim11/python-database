from pydantic import BaseModel
from api.models.base import Base


class RegionBase(BaseModel):
    name: str


class RegionModify(RegionBase):
    pass


class Region(RegionBase, Base):
    id: int
