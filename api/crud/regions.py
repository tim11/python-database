from typing import List, Dict

from sqlalchemy.orm import Session, Query
from sqlalchemy.exc import IntegrityError
from api.common.exceptions import CrudException
from core import orm


class Crud:
    @staticmethod
    def all(session: Session) -> List[orm.Regions]:
        return session.query(orm.Regions).all()

    @staticmethod
    def one(session: Session, _id: int, only_query = False) -> orm.Regions | Query | None:
        query = session.query(orm.Regions).filter(orm.Regions.id == _id)
        return query if only_query else query.first()

    @staticmethod
    def create(session: Session, data: Dict) -> orm.Regions | CrudException:
        item = orm.Regions(**data)
        try:
            session.add(item)
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        session.refresh(item)
        return item

    @staticmethod
    def update(session: Session, _id: int, data: Dict) -> orm.Regions | CrudException | None:
        item = Crud.one(session, _id)
        if not item:
            return
        item.fill(data)
        try:
            session.add(item)
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        session.refresh(item)
        return item

    @staticmethod
    def delete(session: Session, _id: int) -> bool | CrudException | None:
        item = Crud.one(session, _id, True)
        if not item.first():
            return
        try:
            item.delete()
            session.commit()
        except IntegrityError as e:
            session.rollback()
            raise CrudException(str(e.__cause__))
        return True
