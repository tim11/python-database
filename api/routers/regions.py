from typing import List
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from api.dependencies import get_token_header, get_session
from api.crud import RegionsCrud
from api.common.exceptions import CrudException
from api import models


router = APIRouter(
    prefix='/regions',
    tags=['regions'],
    responses={404: {'description': 'Item not found'}}
)


@router.get('', response_model=List[models.Region])
async def get_all(session: Session = Depends(get_session)):
    return RegionsCrud.all(session)


@router.get('/{region_id}', response_model=models.Region)
async def get_by_id(region_id: int, session: Session = Depends(get_session)):
    item = RegionsCrud.one(session, region_id)
    if not item:
        raise HTTPException(status_code=404, detail="Region not found")
    return item


@router.post(
    '',
    tags=['operation'],
    responses={403: {"description": "Operation forbidden"}},
    status_code=201,
    dependencies=[Depends(get_token_header)],
    response_model=models.Region
)
async def create(region: models.RegionModify, session: Session = Depends(get_session)):
    try:
        item = RegionsCrud.create(session, region.dict())
        return item
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e))


@router.put(
    '/{region_id}',
    tags=['operation'],
    responses={403: {"description": "Operation forbidden"}},
    dependencies=[Depends(get_token_header)],
    response_model=models.Region
)
async def update(region_id: int, region: models.RegionModify, session: Session = Depends(get_session)):
    try:
        item = RegionsCrud.update(session, region_id, region.dict(exclude_unset=True))
        if not item:
            raise HTTPException(status_code=404, detail="Region not found")
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e.__cause__))
    return item


@router.delete(
    '/{region_id}',
    tags=['operation'],
    responses={403: {"description": "Operation forbidden"}},
    dependencies=[Depends(get_token_header)],
    status_code=204
)
async def delete(region_id: int, session: Session = Depends(get_session)):
    try:
        result = RegionsCrud.delete(session, region_id)
        if result is None:
            raise HTTPException(status_code=404, detail="Region not found")
        if not result:
            raise HTTPException(status_code=500, detail="Region wasn't be deleted")
    except CrudException as e:
        raise HTTPException(status_code=400, detail=str(e.__cause__))
