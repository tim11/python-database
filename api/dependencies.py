from typing import Annotated
from fastapi import Header, HTTPException
from sqlalchemy.orm import Session
from core import Database


async def get_token_header(x_token: Annotated[str, Header()]):
    print(x_token)
    if x_token != '12345':
        raise HTTPException(status_code=400, detail='X-Token header invalid')


def get_session() -> Session | Exception:
    return Database.get_session()
