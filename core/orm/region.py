from core.orm import Base
from sqlalchemy import Column, Integer, String


class Regions(Base):
    __tablename__ = 'regions'

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(String, unique=True, index=True)
