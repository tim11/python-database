import datetime
from sqlalchemy import Column, DateTime
from sqlalchemy.ext.declarative import declarative_base

DeclarativeBase = declarative_base()


class Base(DeclarativeBase):
    __abstract__ = True

    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)

    def fill(self, values: dict):
        for key, value in values.items():
            if hasattr(self, key):
                setattr(self, key, value)

