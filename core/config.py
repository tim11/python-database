from typing import Any
from environs import Env
from .utils import File


class Config:
    __env: Env

    def __init__(self, config_file: str):
        if not File.exists(config_file):
            raise Exception('Problem with config file')
        self.__env = Env()
        self.__env.read_env(config_file)
        self.check()

    def check(self) -> bool | Exception:
        return True

    def get(self, param: str, default: Any = None) -> Any:
        return self.__env(param, default=default)

    @property
    def dsn(self) -> str | None:
        return None
