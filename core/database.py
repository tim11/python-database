from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from sqlalchemy.engine import Engine
from core.orm import Base as OrmBase
from core.config import Config


class Database:
    __session: Session = None
    __engine: Engine = None

    @staticmethod
    def config(config: Config):
        if Database.__session:
            Database.__session.close()
            Database.__engine = None
        Database.__engine = create_engine(config.dsn, echo=bool(config.get('DEBUG', False)))
        Database.__session = Session(autoflush=False, bind=Database.__engine)
        OrmBase.metadata.create_all(bind=Database.__engine)

    @staticmethod
    def get_session() -> Session | Exception:
        if not Database.__session:
            raise Exception('Please config db connection')
        return Database.__session

