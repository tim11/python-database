from os import path


class File:
    @staticmethod
    def exists(filename: str) -> bool:
        return path.exists(filename) and (path.isfile(filename) or path.islink(filename))
