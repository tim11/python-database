import React from "react";
import {api} from '../common/Api';

class Regions extends React.Component {
    state = {
        items: [],
    }
    componentDidMount() {
        api.regions.all().then(res => {
            const items = res.data;
            this.setState({ items });
        });
    }

    render() {
        return (
            <>
                <div className="Regions">
                    Regions
                </div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Create at</th>
                            <th>Updated at</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.items.map(item =>
                            <tr>
                                <td scope="row">{item.id}</td>
                                <td>{item.name}</td>
                                <td>{item.created_at}</td>
                                <td>{item.updated_at}</td>
                            </tr>
                        )
                    }
                    </tbody>
                </table>
            </>
        );
    }
}

export default Regions
