import React from "react";
import {
    createBrowserRouter,
    createRoutesFromElements,
    Route,
    RouterProvider,
    Link,
    useLocation,
    Outlet
} from 'react-router-dom';

import Home from './routes/Home';
import Regions from './routes/Regions';

const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path="/" element={<Layout/>}>
            <Route index element={<Home/>}/>
            <Route path="regions" element={<Regions/>}/>
        </Route>
    )
);

function Layout() {
    // let [historyIndex, setHistoryIndex] = React.useState(
    //     window.history.state?.idx
    // );
    // let location = useLocation();
    // React.useEffect(() => {
    //     setHistoryIndex(window.history.state?.idx);
    // }, [location]);
    // React.useEffect(() => {
    //     document.title = location.pathname;
    // }, [location]);
    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid">
                    <button
                        className="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation"
                    >
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <Link to={'/'} className="nav-link" aria-current="page">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link to={'/regions'} className="nav-link" aria-current="page">Regions</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <main className="container-fluid">
                <Outlet/>
            </main>
        </>
    );
}

export default function App() {
    return <RouterProvider router={router}/>;
}
