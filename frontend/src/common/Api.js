import axios from 'axios';

class Base {
    client = null;
    constructor(client) {
        this.client = client;
    }
}

class Regions extends Base{
    all() {
        return this.client.get('/regions');
    }
    one(id) {
        return this.client.get('/regions/' + id);
    }
}

class Api {
    regions = null;
    constructor() {
        let client = axios.create({
            baseURL: 'http://127.0.0.1:8000',
        });
        this.regions = new Regions(client);
    }
}

export const api = new Api();
